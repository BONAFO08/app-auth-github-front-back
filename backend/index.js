import 'dotenv/config.js'
import express from 'express';
import passport from 'passport'
import GitHubStrategy from 'passport-github2';


const app = express();
const port = 3000;
//ALOJA LA INFORMACION DE FORMA TEMPORAL DE LOS USUARIOS CONECTADOS
let docum = [];
let url_frontEnd = 'https://myzero.loca.lt';



app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


app.use(express.urlencoded({ extended: true }));
app.use(express.json());


passport.serializeUser((user, cb) => {
    cb(null, user)
})

passport.deserializeUser((user, cb) => {
    cb(null, user)
})



passport.use(new GitHubStrategy({
    clientID: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    callbackURL: "https://mytokensito.loca.lt/auth/github/mytokensito"
},
    async function (accessToken, refreshToken, profile, done) {
        docum.push(profile);
        return done(null, profile)
    }

));



app.get('/', (req, res) => {
    res.send('READY FOR ACCION')
})

app.get("/dataUsers/:id", (req, res) => {
    //BUSCA EL URUARIO CUYO GITLAB CODE CORRESPONDE
    let aux = docum.filter(arr => arr.authCode == req.params.id);
    // Y LO ELIMINA DEL ARR
    let index = docum.indexOf(aux[0]);
    docum.splice(index,1)
    res.json(aux);
    
});


app.get('/auth/github',
    passport.authenticate('github'/*, { scope: ["profile"] }*/));


app.get('/auth/github/mytokensito',
    passport.authenticate('github', { failureRedirect: '/login' }),
    async function (req, res) {

        //UTILIZANDO LA DATA DE REQ.USER BUSCANDO EN EL ARR EL USUARIO 
        //LOGEADO AL QUE EL CODE CORRESPONDE
        let aux = docum.filter(arr => arr == req.user);
        //Y GUARDANDO EL CODE EN LA DATA DEL ARR
        aux[0].authCode = req.query.code;
        res.redirect(`${url_frontEnd}/user/${req.query.code}`)
    });

app.listen(port, () => {
    console.log(`Server on in port ${port}`);
});

