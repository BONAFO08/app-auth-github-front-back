import 'dotenv/config.js'
import express from 'express';
import path from 'path';


const app = express();
const port = 4000;

//CARGANDO LOS ARCHIVOS ESTATICOS (HTML,CSS,JS)
app.use(express.static(process.env.DIR));


app.get('/', (req, res) => {
    res.sendFile(path.join(process.env.DIR + '/index.html'))
})


app.get('/user/:id', (req, res) => {
    res.sendFile(path.join(process.env.DIR + '/userData.html'))
})



app.listen(port, () => {
    console.log(`Server on in port ${port}`);
});

